<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="./styles/styles.css?<?php echo date('l jS \of F Y h:i:s A'); ?>">
        <script src="./scripts/main.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>
        <meta charset="UTF-8">
    </head>
    <body onload="onLoad()">
        <canvas id="GUICanvas">
        Your browser does not support the canvas element.
        </canvas>
<form id="form"  class="slidecontainer">
    Reload🔄:<input type="checkbox" id="reload" value="true" >
    Target🎯:<input type="checkbox" id="target" value="true">
    Seeing Distance👀:<input type="checkbox" id="seeingDistance" value="true">
    Seeing line👀:<input type="checkbox" id="seeingLine" value="true">
    Comfort Zone🤗:<input type="checkbox" id="comfortZone" value="true">
    Draw sea🌊:<input type="checkbox" id="drawSea" value="true" checked>
    Food Sensor🍔:<input type="checkbox" id="foodSensor" value="true">
    Food line🍔:<input type="checkbox" id="foodLine" value="true">
    <p>Max Seeing Distance <span id="maxSeeingDistanceValueValue">400</span></p><input type="range" id="maxSeeingDistanceValue" min="0" max="2000" step="1" value="400" style="width: 200px;" oninput="DisplayChange(this.value,'maxSeeingDistanceValueValue')">
    <p>Max ComfortZone <span id="maxComfortZoneValueValue">100</span></p><input type="range" id="maxComfortZoneValue" min="0" max="1000" step="1" value="100" style="width: 200px;" oninput="DisplayChange(this.value,'maxComfortZoneValueValue')">
    <p>Max Fish <span id="maxFishValueValue">300</span></p><input type="range" id="maxFishValue" min="0" max="1000" step="1" value="300" style="width: 200px;" oninput="DisplayChange(this.value,'maxFishValueValue')">
    <p>Max Radius <span id="maxRadiusValueValue">10</span></p><input type="range" id="maxRadiusValue" min="0" max="1000" step="1" value="10" style="width: 200px;" oninput="DisplayChange(this.value,'maxRadiusValueValue')">
</form>
    </body>
</html>
