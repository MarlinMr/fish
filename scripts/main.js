// Bytt ut bevegelse med fart vektor som endres n�r de ser noen andre
var worldWidth = 4*1280;
var worldHeight = 4*720;
var seaColor = '00ff00';
var fish = [];
var maxFish = 100;
var maxSeeingDistance = 100;
var maxFoodSensor = 1200;
var distArray = [];
var maxRadius = 10;
var maxSwimSpeed = 5;
var maxComfortZone = 100;
var twopi = 2 * Math.PI;
var maxConformity = twopi/100;
var foodList = [];
for (i=0;i<maxFish;i++){distArray[i] = [];}

function onLoad(){
	canvas = document.getElementById("GUICanvas");
	canvas.width = worldWidth;
	canvas.height = worldHeight;
	ctx = canvas.getContext("2d");
	loadVariables();
	for (i=0; i<Number(maxFishValue.value); i++){fish[i] = generateFish();}
    canvas.addEventListener("mousedown", function(event){
	var rect = canvas.getBoundingClientRect();
	var ratiox = rect.width/worldWidth;
	var ratioy = rect.height/worldHeight;
	var x = event.x/ratiox;
	var y = event.y/ratioy;
	foodList.push({x:x,y:y});}, false);
	window.requestAnimationFrame(gameLoop);
	
}

function gameLoop(){
	if (document.getElementById("drawSea").checked){
		drawSea();
	}
	drawFish();
	fishSee();
	drawFood();
	window.requestAnimationFrame(gameLoop);
	if (document.getElementById("reload").checked){
	    for (i=0; i<Number(maxFishValue.value); i++){fish[i] = generateFish();}
	}
	
}

function fishSee(){
    if(fish.length>Number(maxFishValue.value)){
        fish.length = Number(maxFishValue.value);
    }else if(fish.length<Number(maxFishValue.value)){
        for(i=fish.length; i<Number(maxFishValue.value); i++){
            fish[i] = generateFish();
        }
    }
    oldFish = fish;
    for (i=0;i<fish.length;i++){
        if ((fish[i].x < (0)) || (fish[i].x > (worldWidth))){fish[i].x = (worldWidth + fish[i].x) % worldWidth;}
        if ((fish[i].y < (0)) || (fish[i].y > (worldHeight))){fish[i].y = (worldHeight + fish[i].y) % worldHeight;}
        else {
            fish[i].seeingList.length = 0;
            fish[i].foodList.length = 0;
            //// FoodList
            for (j=0; j<foodList.length; j++){
                xdist = fish[i].x - foodList[j].x;
                ydist = fish[i].y - foodList[j].y;
                absDist = Math.sqrt((xdist*xdist) + (ydist*ydist));
                if (absDist < fish[i].foodSensor){
                    fish[i].foodList.push({x:foodList[j].x, y:foodList[j].y, dist:absDist});
                    fish[i].swimDirection = Math.atan2(foodList[j].y - fish[i].y, foodList[j].x - fish[i].x);
                    if (document.getElementById("foodLine").checked){
                        drawLine({x:fish[i].x, y:fish[i].y}, {x:foodList[j].x, y:foodList[j].y}, "#ffff00");
                    }
                    if(((Math.abs(fish[i].x - foodList[j].x))<fish[i].radius) && ((Math.abs(fish[i].y - foodList[j].y))<fish[i].radius)){foodList.splice(j, 1);}
                }
            }
            //// ConformDirection
            for (j=0; (j<oldFish.length) && (fish[i].foodList.length===0); j++){
                if (j != i){
                    xdist = fish[i].x - oldFish[j].x;
                    ydist = fish[i].y - oldFish[j].y;
                    absDist = Math.sqrt((xdist*xdist) + (ydist*ydist));
                    if (absDist < fish[i].seeingDistance){
                        fish[i].seeingList.push({x:fish[j].x, y:fish[j].y, dist:absDist, swimDirection:oldFish[j].swimDirection, conformity:oldFish[j].conformity});
                        if ((fish[i].conformity < oldFish[j].conformity) && (fish[i].swimDirection < oldFish[j].swimDirection)){
                            fish[i].swimDirection = Math.abs((fish[i].swimDirection + fish[i].conformity) % twopi);
                        }else if ((fish[i].conformity < oldFish[j].conformity) && (fish[i].swimDirection > oldFish[j].swimDirection)){
				            fish[i].swimDirection = Math.abs((fish[i].swimDirection - fish[i].conformity) % twopi);
                        }
                        if (document.getElementById("seeingLine").checked){
                            ctx.lineWidth = 1;
                            drawLine({x:fish[i].x, y:fish[i].y}, {x:oldFish[j].x, y:oldFish[j].y}, color);
                        }
                    }
                }
            }
            //// ComfortZone
            for (k=0; k<fish[i].seeingList.length; k++){
                color = "#0000ff";
                if (fish[i].seeingList[k].dist < fish[i].comfortZone){
                    fish[i].x = fish[i].x + ((fish[i].x - fish[i].seeingList[k].x)/100)*fish[i].swimSpeed;
                    fish[i].y = fish[i].y + ((fish[i].y - fish[i].seeingList[k].y)/100)*fish[i].swimSpeed;
                    color = "#ff0000";
                }
            }
        }
    moveFish(fish[i]);}
}

function moveFish(fish){
    fish.x = fish.x + Math.cos(fish.swimDirection)*fish.swimSpeed;
    fish.y = fish.y + Math.sin(fish.swimDirection)*fish.swimSpeed;
    fish.targetx = fish.x + Math.cos(fish.swimDirection)*fish.swimSpeed*20;
    fish.targety = fish.y + Math.sin(fish.swimDirection)*fish.swimSpeed*20;
}


function drawSea(){
	ctx.fillStyle =  "#339CFF";
	ctx.fillRect(0, 0, canvas.width, canvas.height);
}

function drawFish(){
	for (i=0;i<fish.length;i++){
	    a = (Math.PI*2 + fish[i].swimDirection) % (Math.PI*2);
	    s = (Math.floor(a*255/twopi)).toString(16);
        while (s.length<2){s = "0" + s;}
		drawCircle({x:fish[i].x, y:fish[i].y}, fish[i].radius, "#ff" + s + "00");
        ctx.beginPath();
        ctx.moveTo(fish[i].x, fish[i].y);
        ctx.lineTo(fish[i].x+(fish[i].x-fish[i].targetx), fish[i].y+(fish[i].y-fish[i].targety));
        ctx.strokeStyle = "#ff" + s + "00";
		ctx.lineWidth = fish[i].radius;
        ctx.stroke();
		//drawLine({x:fish[i].x, y:fish[i].y}, {x:fish[i].x+(fish[i].x-fish[i].targetx), y:fish[i].y+(fish[i].y-fish[i].targety)}, fish[i].fillStyle);
		if (document.getElementById("seeingDistance").checked){
			ctx.beginPath();
			ctx.arc(fish[i].x, fish[i].y, fish[i].seeingDistance, 0, twopi, true);
			ctx.strokeStyle = "#000000";
			ctx.lineWidth = 1;
			ctx.stroke();
		}
		if (document.getElementById("comfortZone").checked){
			ctx.beginPath();
			ctx.arc(fish[i].x, fish[i].y, fish[i].comfortZone, 0, twopi, true);
			ctx.strokeStyle = "#00ff00";
			ctx.lineWidth = 1;
			ctx.stroke();
		}
		if (document.getElementById("target").checked){
		    ctx.lineWidth = 1;
			drawLine({x:fish[i].x, y:fish[i].y}, {x:fish[i].targetx, y:fish[i].targety}, "#ff0000");
		}
		if (document.getElementById("foodSensor").checked){
			ctx.beginPath();
			ctx.arc(fish[i].x, fish[i].y, fish[i].foodSensor, 0, twopi, true);
			ctx.strokeStyle = "#ff00ff";
			ctx.lineWidth = 1;
			ctx.stroke();
		}
	}
}

function drawFood(){
	for (i=0;i<foodList.length;i++){
		drawCircle({x:foodList[i].x,y:foodList[i].y},5,"#00ff00");
	}
}

function drawLine(a, b, color){
	ctx.beginPath();
	ctx.moveTo(a.x, a.y);
	ctx.lineTo(b.x, b.y);
	ctx.strokeStyle = color;
	ctx.stroke();
}

function drawCircle(a, radius, color){
	ctx.beginPath();
	ctx.arc(a.x, a.y, radius, 0, twopi, true);
	ctx.fillStyle = color;
	ctx.fill();
}

function generateFish(){
	localFish = {x:Math.random()*worldWidth,
		y:Math.random()*worldHeight,
		radius:Math.random()*maxRadius,
		fillStyle:"#" + Math.floor(Math.random()*16581375).toString(16),//"#FFAA00",
		seeingDistance:Math.random()*maxSeeingDistance,
		swimSpeed:Math.random()*maxSwimSpeed,
		targetx:this.x,
		targety:this.y,
		comfortZone:Math.random()*maxComfortZone,
		swimDirection:Math.random()*2*Math.PI,
		conformity:Math.random()*maxConformity,
		foodSensor:Math.random()*maxFoodSensor,
		foodList:[],
		seeingList:[]
	}
	localFish.comfortZone = localFish.comfortZone % localFish.seeingDistance;
return localFish;}

function DisplayChange(newvalue,element) {
    document.getElementById(element).innerHTML = newvalue;
}
function loadVariables(){
    maxSeeingDistance       = Number(maxSeeingDistanceValue.value);	
    maxcomfortZone          = Number(maxComfortZoneValue.value);	
    maxRadius               = Number(maxRadiusValue.value);	
    maxFish                 = Number(maxFishValue.value);	
}
